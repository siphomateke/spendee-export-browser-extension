function downloadData(data, filename) {
  const blob = new Blob([data], { type: `text/csv;charset=utf-8` });
  const fullFilename = `${filename}.csv`;
  const downloadUrl = URL.createObjectURL(blob);
  return browser.downloads.download({
    url: downloadUrl,
    filename: fullFilename,
    saveAs: false,
  });
}

browser.runtime.onMessage.addListener((message) => {
  if (message.command === 'download') {
    downloadData(message.data, message.filename);
  }
});