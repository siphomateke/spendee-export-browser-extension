# Spendee Export Browser Extension

Browser extension that adds buttons to the Spendee web app to export categorical incomes and expenses as CSV files.

The "Export to CSV" button in the screenshot below is what this extension adds.

![Screenshot](./screenshot.png)
