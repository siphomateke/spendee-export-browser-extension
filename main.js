function waitForElement(doc, selector, callback, once = false) {
  const el = doc.querySelector(selector);
  if (el) {
    callback(el);
  }
  const observer = new MutationObserver((mutations) => {
    for (const mutation of mutations) {
      if (mutation.target.matches(selector)) {
        callback(mutation.target);
        if (once) {
          observer.disconnect();
          break;
        }
      }
    }
  });
  observer.observe(doc, {
    childList: true,
    subtree: true,
  });
}

function waitForElementPromise(doc, selector) {
  return new Promise((resolve) => {
    waitForElement(doc, selector, resolve, true);
  });
}

function createSpendeeButton() {
  const button = document.createElement('button');
  button.type = 'button';
  button.classList.add(...['egUi', 'addInverse', '_3SdL', '_2_oj', '_1mpn', '_3dgm', 'export-csv-button']);
  const text = document.createElement('span');
  text.classList.add('A4xM');
  text.textContent = 'Export to CSV';
  button.append(text);
  button.style.float = 'right';
  return button;
}

function getCsvFromCategoryTable(table) {
  return Array.from(table.querySelectorAll('a._1V3G')).map(el => {
    const category = el.querySelector('div:nth-child(1) > div:nth-child(2) > div:nth-child(1)').textContent.trim();
    let amount = el.querySelector('div:nth-child(1) > div:nth-child(3)').textContent.trim();
    amount = amount.replace(',', '');
    const currencyMatch = amount.match(/\s([a-zA-Z]+)$/);
    let currency = '';
    if (currencyMatch && currencyMatch.length === 2) {
      currency = currencyMatch[1];
      amount = amount.replace(` ${currency}`, '');
    }
    return `${category}\t${amount}\t${currency}`;
  }).join('\n');
}

function initCategoryCard(cardSelector, filename) {
  const card = document.querySelector(cardSelector);
  const cardContent = card.querySelector('div:nth-child(1) > div:nth-child(1)');
  const headerButtons = cardContent.querySelector('div:nth-child(1) > div:nth-child(2)');
  const existingButton = card.querySelector('.export-csv-button');
  if (existingButton) {
    return;
  }
  const button = createSpendeeButton();
  headerButtons.append(button);
  button.addEventListener('click', () => {
    const table = cardContent.querySelector('div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)');
    const csv = getCsvFromCategoryTable(table);
    browser.runtime.sendMessage({ command: 'download', data: csv, filename });
  });
}

async function init() {
  try {
    waitForElement(document.body, `div._2qN1 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2), ._3fS2.o-cw._2oRk`, (el) => {
      initCategoryCard('div._2qN1:nth-child(1)', 'Income by category');
      initCategoryCard('div._2qN1:nth-child(2)', 'Expenses by category');
    });
  } catch (error) {
    console.log(error);
  }
}

// const cardsContainerSelector = 'div._2AsN:nth-child(5)';
// const cardSelector = '._2AsN,._3fS2.o-cw._2oRk._2qN1';
// function waitForCards() {
//   const cardsContainerEl = document.querySelector(cardsContainerSelector);
//   waitForElement(cardsContainerEl, cardSelector, init);
// }

// function waitForCardsContainer() {
//   const cardsContainerLoadedSelector = cardsContainerSelector + ',._2ylm > div:nth-child(1)';
//   waitForElement(document.body, cardsContainerLoadedSelector, waitForCards, true);
// }

// waitForCardsContainer();

init();